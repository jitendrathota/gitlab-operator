# Known limitations

Below are known limitations of the GitLab Operator.

## Certain components not supported

Below is a list of unsupported components:

- Praefect: [#136](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/issues/136)

### Certain components not recommended for production use

Below is a list of components that are not yet recommended for production when
deployed by the GitLab Operator:

- KAS: [#353](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/issues/353)
